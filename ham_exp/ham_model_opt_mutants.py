import numpy as np
import scipy as sc
from scipy.optimize import minimize
from ham_model_functions import *
import sys
import os

### Reoptimisation of CLV and HAM parameters including mutants (clv lof, ham lof) of existing parameter set. ### 

# Line in file of parameter set.
param_id = int(sys.argv[1])

''' import template '''
template = np.loadtxt("real_template2")
X = template[:,0]
Y = template[:,1]
Z = template[:,2]
S = template[:,3]
Vol = template[:,4]
L1 = template[:,5]
Sink = template[:,6]
#TC = template[:,7]
TW = template[:,8]
meristem = template[:,9]

''' import CLV template (overlap with WUS domain) '''
Ctemp = np.loadtxt("template_CLV")
TC = Ctemp[:,4]


''' import contact matrix '''
Contacts = read_contacts_real("real_template_contacts",len(X))
Sum_Contacts = np.zeros(len(X))
for i,c in enumerate(Contacts):
    Sum_Contacts[i] = sum(c)

Sink *= sum(Sum_Contacts)/len(Sum_Contacts)


''' define target HAM domain '''
Zmer = Z*meristem
max_z = np.amax(Zmer)
z_ind = Zmer.argmax()
c = max_z
x0 = X[z_ind]
y0 = Y[z_ind]
a = 70**2/c

TH = np.zeros(len(X))
for i in range(len(X)):
    if Z[i] <=  -((X[i] - x0)**2/a + (Y[i] - y0)**2/a) + (c-12):
        TH[i] += 1

S2 = np.zeros(len(S))
for i in range(len(S)):
    if Y[i]>Y[z_ind]: S2[i]=S[i]

    
''' finding data, rows and columns of contact matrix & diagonal for diffusion '''
Contacts_scaled = np.zeros(Contacts.shape)
for i in range(len(Vol)):
    Contacts_scaled[i,:] = Contacts[i,:] / Vol[i]
        
rows, cols, data = [],[],[]
for i in range(len(Contacts[0])):
    for j in range(i+1,len(Contacts[1])):
        if Contacts_scaled[i,j] != 0:
            rows.append(i)
            rows.append(j)
            cols.append(j)
            cols.append(i)
            data.append(Contacts_scaled[i,j])
            data.append(Contacts_scaled[j,i])
data = np.array(data)
rows = np.array(rows)
cols = np.array(cols)

diag0 = Sink + Sum_Contacts/Vol


'''WUSCHEL'''  
def stable_w(parameters):
    global cyt, ahk, bW, c, sW
    cyt = diff_real_rowscols(parameters[4],parameters[5],parameters[6],L1,data,rows,cols,diag0)
    ahk = diff_real_rowscols(parameters[7],parameters[8],parameters[9],L1,data,rows,cols,diag0) 
    bW = hillfun(parameters[0],[(parameters[1],ncytW,cyt)],[(parameters[2],nahkW,ahk)],parameters[3])
    c = diff_real_rowscols(parameters[11],parameters[12],parameters[13],TC,data,rows,cols,diag0)
    sW = hillfun(parameters[0],[(parameters[1],ncytW,cyt)],[(parameters[2],nahkW,ahk),(parameters[10],ncW,c)],parameters[3])
    # vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc = parameters
    return cyt, ahk, bW, c, sW


''' CLV3 '''
# mode=0: wildtype, mode=1: clv lof, mode=2: ham lof
def stable_c(parameters,mode=0,limit=100):
    global lh,H,wus,ham,dim,lc,C
    vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC = parameters
    lh = diff_real_rowscols(plh,glh,dlh,L1,data,rows,cols,diag0)
    H = hillfun(vH,[],[(klhH,nlhH,lh)],gH)
    wus,ham,dim = np.zeros(len(X)),np.zeros(len(X)),np.zeros(len(X))
    if mode == 1: pwus*=1.5
    if mode == 2: pham = 0
    stable = 10000
    i_stable = 0
    while stable > 1e-10 and i_stable <= limit:
        i_stable += 1
        print '.',
        M,jdata,jrows,jcols = jac(wus,ham,dim,gwus,dwus,gham,dham,gdim,ddim,forw,back,diag0)
        g = vec(W,H,wus,ham,dim,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,diag0,M,jdata,jrows,jcols)
        v = sc.sparse.linalg.spsolve(M,g)
        v_old = np.concatenate((wus,ham,dim))
        stable = np.sum((v-v_old)**2)
        wus = v[0:len(X)]
        ham = v[len(X):2*len(X)]
        dim = v[2*len(X):3*len(X)]
    if i_stable == limit:
        wus,ham,dim = np.zeros(len(X)),np.zeros(len(X)),np.zeros(len(X))
    C = shea(vC,[(kwusC,wus),(klcC,lc)],[(kdimC,dim)],gC)
    return lh,H,wus,ham,dim,C
    

def cost_c(parameters):
    global count_opt; count_opt +=1
    _,H,_,_,_,C = stable_c(parameters); print "wild",
    _,_,_,_,_,C1 = stable_c(parameters,mode=1); print "Clof",
    _,_,_,_,_,C2 = stable_c(parameters,mode=2); print "Hlof",
    diff1 = np.sum((meristem*C-TC)**2)
    diff2 = np.sum((meristem*H-TH)**2)/30
    diff3 = (((np.sum(C1)/np.sum(C))-1.5)**2) / 5
    diff4 = np.sum((W-C2)**2)/50.
    diff =  diff1 + diff2 + diff3 + diff4
    print "\n",count_opt,"C", diff, diff1, diff2, diff3, diff4
    if diff != diff: sys.exit() 
    return diff


''' clv3 '''
def f_c(parameters):
    global c
    pc, gc, dc = parameters
    c = diff_real_rowscols(pc,gc,dc,C,data,rows,cols,diag0)
    diff = sum((c-Tc)**2)
    print diff
    return diff
    
    
''' minimising function '''
def optimize(f,nb_params,thr1,thr2,first_params=np.array([]),bounds=3,max_count=100,max_count2=1000):
    count = 0
    bounds = [(10**-bounds,10**bounds) for e in range(nb_params+first_params.shape[0])]
    diff = np.finfo('d').max
    while diff >= thr2 and count<max_count:
        count2 = 0
        count += 1
        diff = np.finfo('d').max
        while diff >= thr1 and count2<max_count2:
            params = np.append(first_params,np.random.random(nb_params)* 10.**np.random.random_integers(-2,2,nb_params))
            diff = f(params)
            print "r", diff
            count2 += 1
        params = minimize(f,params,method='L-BFGS-B',bounds=bounds).x
        diff = f(params)
        print "*****", diff
    return params,count
    
    
''' Hill coefficients '''
ncytW=7.25968619416
nahkW=1.99109438845
ncW=6.66419523049
nlcC=9.20473637339
nwusC=5.35873341322
nlhH=6


''' load parameters '''
vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc, vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC = read_params('params_ham',param_id)

Wparams = [vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc]
bwparams = [vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk]
swparams = [kcW, pc, gc, dc]
Cparams = [vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC]


''' optimisation '''
# WUS is computed
cyt, ahk, bW, c, sW = stable_w(Wparams)
W = sW.copy()

# CLV & HAM cost function is minimised
count_opt=0
bounds = [(10**-4,10**4) for e in range(len(Cparams))]
Cparams = minimize(cost_c,Cparams,method='L-BFGS-B',bounds=bounds).x
vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC = Cparams

# clv peptide is reoptimised
Tc = np.copy(c)
params,count = optimize(f_c,3,1000,1000)
diff_c = f_c(params)
pc, gc, dc = params
swparams[1:4] = params

# parameters are saved
opt_params_ns = np.concatenate((bwparams,swparams,Cparams))
print_full_params('params_ham_mutants_notstab',opt_params_ns)


''' feedback loop '''
if count < 100:
    W = bW.copy()
    lh,H,wus,ham,dim,bC = stable_c(Cparams)
    step_size = .1
    diffs = 999999999999
    i = -1
    while diffs > 10**-10 and i<500:
        i+=1
        lh,H,wus,ham,dim,bC = stable_c(Cparams)
        c = diff_real_rowscols(pc,gc,dc,bC,data,rows,cols,diag0)
        W = hillfun(vW,[(kcytW,ncytW,cyt)],[(kahkW,nahkW,ahk),(kcW,ncW,c)],gW)*step_size + (1-step_size)*bW
        lh,H,wus,ham,dim,C = stable_c(Cparams)
        if np.sum(C) < np.sum(bC):
            bW = W.copy()
            print '..',step_size
        else:
            W = bW.copy()
            step_size*=.1
            print 'xx',step_size
        W_deriv = np.sum(vW*cyt**ncytW/(kcytW**ncytW + cyt**ncytW)*kahkW**nahkW/(kahkW**nahkW + ahk**nahkW)*kcW**ncW/(kcW**ncW+c**ncW)-gW*W)
        C_deriv = np.sum(vC*(kwusC*wus)/(1+kwusC*wus+kdimC*dim)-gC*C)
        diffs = abs(W_deriv) + abs(C_deriv) 
        print i, diffs
	
    # parameters are saved if feedback loop was successful	        
    if i < 500 and i > 0:
        opt_params = np.concatenate((bwparams,swparams,Cparams))
        print_full_params('params_ham_mutants',opt_params)
