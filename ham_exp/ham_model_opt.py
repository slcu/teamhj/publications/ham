import numpy as np
import scipy as sc
from scipy.optimize import minimize
from ham_model_functions import *
import sys
import os


''' WUS parameters are saved after WUS optimisation '''
''' CLV parameter set & expression domains are saved during CLV optimisation (every time the cost function decreases) '''
file_ext = sys.argv[1]
Wparams_name = "params_W"+file_ext
Cparams_name = "params_C"+file_ext
file_name="domains_opt"+file_ext


''' import template '''
template = np.loadtxt("real_template2")
X = template[:,0]
Y = template[:,1]
Z = template[:,2]
S = template[:,3]
Vol = template[:,4]
L1 = template[:,5]
Sink = template[:,6]
#TC = template[:,7]
TW = template[:,8]
meristem = template[:,9]

''' import CLV template (overlap with WUS domain) '''
Ctemp = np.loadtxt("template_CLV")
TC = Ctemp[:,4]


''' import contact matrix '''
Contacts = read_contacts_real("real_template_contacts",len(X))
Sum_Contacts = np.zeros(len(X))
for i,c in enumerate(Contacts):
    Sum_Contacts[i] = sum(c)

Sink *= sum(Sum_Contacts)/len(Sum_Contacts)


''' define target HAM domain '''
Zmer = Z*meristem
max_z = np.amax(Zmer)
z_ind = Zmer.argmax()
c = max_z
x0 = X[z_ind]
y0 = Y[z_ind]
a = 70**2/c

TH = np.zeros(len(X))
for i in range(len(X)):
    if Z[i] <=  -((X[i] - x0)**2/a + (Y[i] - y0)**2/a) + (c-12):
        TH[i] += 1

S2 = np.zeros(len(S))
for i in range(len(S)):
    if Y[i]>Y[z_ind]: S2[i]=S[i]

    
''' finding data, rows and columns of contact matrix & diagonal for diffusion '''
Contacts_scaled = np.zeros(Contacts.shape)
for i in range(len(Vol)):
    Contacts_scaled[i,:] = Contacts[i,:] / Vol[i]
        
rows, cols, data = [],[],[]
for i in range(len(Contacts[0])):
    for j in range(i+1,len(Contacts[1])):
        if Contacts_scaled[i,j] != 0:
            rows.append(i)
            rows.append(j)
            cols.append(j)
            cols.append(i)
            data.append(Contacts_scaled[i,j])
            data.append(Contacts_scaled[j,i])
data = np.array(data)
rows = np.array(rows)
cols = np.array(cols)

diag0 = Sink + Sum_Contacts/Vol


'''WUSCHEL'''  
def stable_w(parameters):
    global cyt, ahk, bW, c, sW
    cyt = diff_real_rowscols(parameters[4],parameters[5],parameters[6],L1,data,rows,cols,diag0)
    ahk = diff_real_rowscols(parameters[7],parameters[8],parameters[9],L1,data,rows,cols,diag0) 
    bW = hillfun(parameters[0],[(parameters[1],ncytW,cyt)],[(parameters[2],nahkW,ahk)],parameters[3])
    c = diff_real_rowscols(parameters[11],parameters[12],parameters[13],TC,data,rows,cols,diag0)
    sW = hillfun(parameters[0],[(parameters[1],ncytW,cyt)],[(parameters[2],nahkW,ahk),(parameters[10],ncW,c)],parameters[3])
    # vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc = parameters
    return cyt, ahk, bW, c, sW

def cost_w(parameters):
    cyt,ahk,bW,c,sW = stable_w(parameters)
    diff1 = sum((meristem*sW-TW)**2)
    diff2 = 100/((sum(bW)/sum(sW))**50+1)
    diff = diff1+diff2
    print diff1, diff2, '**', diff
    return diff 

# lower threshold for optimisation
W_lim = 0.6*sum(TW)


''' CLV3 '''
def stable_c(parameters,limit=50):
    global lh,H,wus,ham,dim,lc,C
    vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC = parameters
    lh = diff_real_rowscols(plh,glh,dlh,L1,data,rows,cols,diag0)
    H = hillfun(vH,[],[(klhH,nlhH,lh)],gH)
    wus,ham,dim = np.zeros(len(X)),np.zeros(len(X)),np.zeros(len(X))
    stable = 10000
    i_stable = 0    
    while stable > 1e-10 and i_stable <= limit:
        i_stable += 1
        print '.',
        M,jdata,jrows,jcols = jac(wus,ham,dim,gwus,dwus,gham,dham,gdim,ddim,forw,back,diag0,data,rows,cols)
        g = vec(W,H,wus,ham,dim,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,diag0,M,jdata,jrows,jcols,data,rows,cols)
        v = sc.sparse.linalg.spsolve(M,g)
        v_old = np.concatenate((wus,ham,dim))
        stable = np.sum((v-v_old)**2)
        wus = v[0:len(X)]
        ham = v[len(X):2*len(X)]
        dim = v[2*len(X):3*len(X)]
    if i_stable == limit:
        wus,ham,dim = np.zeros(len(X)),np.zeros(len(X)),np.zeros(len(X))
    C = shea(vC,[(kwusC,wus)],[(kdimC,dim)],gC)
    return lh,H,wus,ham,dim,C
        
def cost_c(parameters): 
    global diff0
    lh,H,wus,ham,dim,C = stable_c(parameters)
    diff1 = sum((meristem*C-TC)**2)
    diff2 = sum((H-TH)**2/30)
    diff = diff1 + diff2
    if diff < diff0:
        diff0 = diff
        print_full_params(Cparams_name,parameters)
        write_init(file_name,[X,Y,Z,S2,TW,W,wus,TC,C,c,TH,H,ham,dim],newman=True)
    print diff1,diff2,"**",diff
    return diff

# lower threshold for optimisation
C_lim = 0.35*(sum(TC)+sum(TH)/30)


''' clv3 '''
def f_c(parameters):
    global c
    pc, gc, dc = parameters
    c = diff_real_rowscols(pc,gc,dc,C,data,rows,cols,diag0)
    diff = sum((c-Tc)**2)
    print diff
    return diff
    
    
''' minimising function '''
# new parameter set is drawn until cost function < thr1, parameter set is optimised, process is repeated until cost function < thr2 (thr1 > thr2)
def optimize(f,nb_params,thr1,thr2,first_params=np.array([]),bounds=3,max_count=100,max_count2=1000):
    count = 0
    bounds = [(10**-bounds,10**bounds) for e in range(nb_params+first_params.shape[0])]
    diff = np.finfo('d').max
    while diff >= thr2 and count<max_count:
        count2 = 0
        count += 1
        diff = np.finfo('d').max
        while diff >= thr1 and count2<max_count2:
            params = np.append(first_params,np.random.random(nb_params)* 10.**np.random.random_integers(-2,2,nb_params))
            diff = f(params)
            print "r", diff
            count2 += 1
        params = minimize(f,params,method='L-BFGS-B',bounds=bounds).x
        diff = f(params)
        print "*****", diff
    return params,count


''' Hill coefficients '''
ncytW=7.25968619416
nahkW=1.99109438845
ncW=6.66419523049
nlhH = 6.0


''' optimisation '''
# 1. WUS cost function is minimised
params, count = optimize(cost_w,14,sum(TW)-.001,W_lim)
vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc = params
Wparams = params
print_full_params(Wparams_name,Wparams)

# 2. CLV & HAM cost function is minimised		
W = sW.copy()
diff0 = 1000
params,count = optimize(cost_c,20,sum(TC)+sum(TH)/30-.001,C_lim)
vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC = params
Cparams = params
		
# 3. clv peptide is optimised towards clv from 1
Tc = np.copy(c)
params,count = optimize(f_c,3,1000,1000)
diff_c = f_c(params)
pc, gc, dc = params
Wparams[11:14] = params

# parameters are saved
params_notstable = np.concatenate((Wparams,Cparams))
print_full_params('params_ham_notstab',params_notstable)

    
''' feedback loop '''
if count < 100:
    W = bW.copy()
    lh,H,wus,ham,dim,bC = stable_c(Cparams)
    step_size = .1
    diffs = 999999999999
    i = -1
    while diffs > 10**-10 and i<500:
        i+=1
        lh,H,wus,ham,dim,bC = stable_c(Cparams)
        c = diff_real_rowscols(pc,gc,dc,bC,data,rows,cols,diag0)
        W = hillfun(vW,[(kcytW,ncytW,cyt)],[(kahkW,nahkW,ahk),(kcW,ncW,c)],gW)*step_size + (1-step_size)*bW
        lh,H,wus,ham,dim,C = stable_c(Cparams)
        c = diff_real_rowscols(pc,gc,dc,C,data,rows,cols,diag0)
        if np.sum(C) < np.sum(bC):
            bW = W.copy()
            print '..',step_size
        else:
            W = bW.copy()
            step_size*=.1
            print 'xx',step_size   
        W_deriv = np.sum(vW*cyt**ncytW/(kcytW**ncytW + cyt**ncytW)*kahkW**nahkW/(kahkW**nahkW + ahk**nahkW)*kcW**ncW/(kcW**ncW+c**ncW)-gW*W)
        C_deriv = np.sum(vC*(kwusC*wus)/(1+kwusC*wus+kdimC*dim)-gC*C)
        diffs = abs(W_deriv) + abs(C_deriv) 
        print i, diffs
	
    # parameters are saved if feedback loop was successful	        
    if i < 500 and i > 0:
        opt_params = np.concatenate((Wparams,Cparams))
        print_full_params('params_ham',opt_params)
