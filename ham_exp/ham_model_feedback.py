import numpy as np
import scipy as sc
from scipy.optimize import minimize
from ham_model_functions import *
import sys
import os


# Line in file of parameter set.
param_id = int(sys.argv[1])


''' import template '''
template = np.loadtxt("real_template2")
X = template[:,0]
Y = template[:,1]
Z = template[:,2]
S = template[:,3]
Vol = template[:,4]
L1 = template[:,5]
Sink = template[:,6]
#TC = template[:,7]
TW = template[:,8]
meristem = template[:,9]

''' import CLV template (overlap with WUS domain) '''
Ctemp = np.loadtxt("template_CLV")
TC = Ctemp[:,4]


''' import contact matrix '''
Contacts = read_contacts_real("real_template_contacts",len(X))
Sum_Contacts = np.zeros(len(X))
for i,c in enumerate(Contacts):
    Sum_Contacts[i] = sum(c)

Sink *= sum(Sum_Contacts)/len(Sum_Contacts)


''' define target HAM domain '''
Zmer = Z*meristem
max_z = np.amax(Zmer)
z_ind = Zmer.argmax()
c = max_z
x0 = X[z_ind]
y0 = Y[z_ind]
a = 70**2/c

TH = np.zeros(len(X))
for i in range(len(X)):
    if Z[i] <=  -((X[i] - x0)**2/a + (Y[i] - y0)**2/a) + (c-12):
        TH[i] += 1

S2 = np.zeros(len(S))
for i in range(len(S)):
    if Y[i]>Y[z_ind]: S2[i]=S[i]

    
''' finding data, rows and columns of contact matrix & diagonal for diffusion '''
Contacts_scaled = np.zeros(Contacts.shape)
for i in range(len(Vol)):
    Contacts_scaled[i,:] = Contacts[i,:] / Vol[i]
        
rows, cols, data = [],[],[]
for i in range(len(Contacts[0])):
    for j in range(i+1,len(Contacts[1])):
        if Contacts_scaled[i,j] != 0:
            rows.append(i)
            rows.append(j)
            cols.append(j)
            cols.append(i)
            data.append(Contacts_scaled[i,j])
            data.append(Contacts_scaled[j,i])
data = np.array(data)
rows = np.array(rows)
cols = np.array(cols)

diag0 = Sink + Sum_Contacts/Vol


'''WUSCHEL'''  
def stable_w(parameters):
    global cyt, ahk, bW, c, sW
    cyt = diff_real_rowscols(parameters[4],parameters[5],parameters[6],L1,data,rows,cols,diag0)
    ahk = diff_real_rowscols(parameters[7],parameters[8],parameters[9],L1,data,rows,cols,diag0) 
    bW = hillfun(parameters[0],[(parameters[1],ncytW,cyt)],[(parameters[2],nahkW,ahk)],parameters[3])
    c = diff_real_rowscols(parameters[11],parameters[12],parameters[13],TC,data,rows,cols,diag0)
    sW = hillfun(parameters[0],[(parameters[1],ncytW,cyt)],[(parameters[2],nahkW,ahk),(parameters[10],ncW,c)],parameters[3])
    # vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc = parameters
    return cyt, ahk, bW, c, sW


''' CLV3 '''
def stable_c(parameters,limit=50):
    global lh,H,wus,ham,dim,lc,C
    vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC = parameters
    lh = diff_real_rowscols(plh,glh,dlh,L1,data,rows,cols,diag0)
    H = hillfun(vH,[],[(klhH,nlhH,lh)],gH)
    wus,ham,dim = np.zeros(len(X)),np.zeros(len(X)),np.zeros(len(X))
    stable = 10000
    i_stable = 0    
    while stable > 1e-10 and i_stable <= limit:
        i_stable += 1
        print '.',
        M,jdata,jrows,jcols = jac(wus,ham,dim,gwus,dwus,gham,dham,gdim,ddim,forw,back,diag0,data,rows,cols)
        g = vec(W,H,wus,ham,dim,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,diag0,M,jdata,jrows,jcols,data,rows,cols)
        v = sc.sparse.linalg.spsolve(M,g)
        v_old = np.concatenate((wus,ham,dim))
        stable = np.sum((v-v_old)**2)
        wus = v[0:len(X)]
        ham = v[len(X):2*len(X)]
        dim = v[2*len(X):3*len(X)]
    if i_stable == limit:
        wus,ham,dim = np.zeros(len(X)),np.zeros(len(X)),np.zeros(len(X))
    C = shea(vC,[(kwusC,wus)],[(kdimC,dim)],gC)
    return lh,H,wus,ham,dim,C
    

''' Hill coefficients '''
ncytW=7.25968619416
nahkW=1.99109438845
ncW=6.66419523049
#nlhH=1.99109438845
nlhH = 6.0


''' load parameters '''
vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc,vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC = read_params('params_ham',param_id)


Wparams = [vW, kcytW, kahkW, gW, pcyt, gcyt, dcyt, pahk, gahk, dahk, kcW, pc, gc, dc]
Cparams = [vH,klhH,gH,plh,glh,dlh,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,vC,kwusC,kdimC,gC]


### clv lof ###
#Wparams[11] = 0.
#pc = 0.


### HAM lof ###
#Cparams[0] = 0.
#vH = 0.


''' feedback loop '''
cyt, ahk, bW, c, sW = stable_w(Wparams)
W = bW.copy()
lh,H,wus,ham,dim,bC = stable_c(Cparams)
step_size = .1
diffs = 999999999999
i = -1

while diffs > 10**-10 and i<500:
    i+=1
    lh,H,wus,ham,dim,bC = stable_c(Cparams)
    c = diff_real_rowscols(pc,gc,dc,bC,data,rows,cols,diag0)
    W = hillfun(vW,[(kcytW,ncytW,cyt)],[(kahkW,nahkW,ahk),(kcW,ncW,c)],gW)*step_size + (1-step_size)*bW
    lh,H,wus,ham,dim,C = stable_c(Cparams)
    c = diff_real_rowscols(pc,gc,dc,C,data,rows,cols,diag0)
    if np.sum(C) < np.sum(bC):
        bW = W.copy()
    else:
        W = bW.copy()
        step_size*=.1
    W_deriv = np.sum(vW*cyt**ncytW/(kcytW**ncytW + cyt**ncytW)*kahkW**nahkW/(kahkW**nahkW + ahk**nahkW)*kcW**ncW/(kcW**ncW+c**ncW)-gW*W)
    C_deriv = np.sum(vC*(kwusC*wus)/(1+kwusC*wus+kdimC*dim)-gC*C)
    diffs = abs(W_deriv) + abs(C_deriv) 
    print i, diffs

write_init("delme",[X,Y,Z,S2,TW,W,wus,TC,C,c,TH,H,ham,dim],newman=True)
os.popen("~/../jeremy/organism/tools/plot/bin/newman -d 3 delme")
