import numpy as np
import random
import scipy as sc
from scipy.optimize import minimize
import sys
import os


def diff_real_rowscols(p,g,d,source,data,rows,cols,diag0):
    """Stable state of a diffusing molecule is computed.

    The stable state of a diffusing molecule including production, degradation 
    and loss due to diffusion out of the bottom of the structure (sink) is 
    determined.

    Parameters
    ----------
    p : float
        Production rate.
    g : float
        Degradation rate.
    d : float
        Diffusion constant.
    source : numpy array
        Vector with source where molecules are produced.
    data : numpy array
        Vector with entries in contact matrix (contact surface/volume between 
        two cells i and j).
    rows : numpy array
        Vector with row indices of entries in contact matrix.
    cols : numpy array
        Vector with column indices of entries in contact matrix.
    diag0 : numpy array
        Sum of sink vector and total contact surface of each cell divided by 
        its volume. 

    Return
    ------
    res : numpy array
        Stable state of molecule in each cell.    
    """
    diag = -d*diag0-g
    diag_rowscols = np.arange(diag0.shape[0])
    M = sc.sparse.coo_matrix((np.concatenate((diag,d*data)),(np.concatenate((diag_rowscols,rows)),np.concatenate((diag_rowscols,cols)))), shape=(diag0.shape[0],diag0.shape[0]))
    M = M.tocsc()
    c0 = -p*source    
    return sc.sparse.linalg.spsolve(M,c0)
    
   
def hillfun(v,a,r,g):
    """Gene expression in stable state is computed.

    The stable state of RNA whose dynamics are described by a Hill function is 
    computed. The RNA undergoes degradation and has activators A and repressors
    R which influence the transcription of the gene.

    Parameters
    ----------
    v : float
        Maximal transcription rate of gene.
    a/r : list of tuples
        Tuple in the form of (k,n,x).
        k: Dissociation constant of activator/repressor.
        n: Hill coefficient of activator/repressor.
        x: Vector with concentration of activator/repressor.
    g : float
        Degradation rate of RNA.

    Return
    ------
    res : numpy array
        Gene expression in stable state in each cell.
    """
    if a != []:
        result = v/g*np.ones(len(a[0][2]))
    else:
        result = v/g*np.ones(len(r[0][2]))
    for (k,n,x) in a:
        result *= x**n/(k**n+x**n)
    for (k,n,x) in r:
        result *= k**n/(k**n+x**n)
    return result
    

def shea(v,a,r,g):
    """Gene expression in stable state is computed.

    The stable state of RNA whose dynamics are described by the Shea Ackers 
    formalism is computed. The RNA undergoes degradation and has activators A 
    and repressors R which influence the transcription of the gene.

    Parameters
    ----------
    v : float
        Maximal transcription rate of gene.
    a/r : list of tuples
        Tuple in the form of (k,x).
        k: Dissociation constant of activator/repressor.
        x: Vector with concentration of activator/repressor.
    g : float
        Degradation rate of RNA.

    Return
    ------
    res : numpy array
        Gene expression in stable state in each cell.
    """
    if a != []:
        result = v/g*np.ones(len(a[0][1]))
        Z0 = np.zeros(len(a[0][1]))
        Z = np.ones(len(a[0][1]))
    else:
        result = v/g*np.ones(len(r[0][1]))
        Z0 = np.zeros(len(a[0][1]))
        Z = np.ones(len(r[0][1]))
    for (k,x) in a:
        Z0 += k*x
        Z += k*x
    for (k,x) in r:
        Z += k*x
    result *= Z0/Z
    return result
        

def jac(wus,ham,dim,gwus,dwus,gham,dham,gdim,ddim,forw,back,diag,data,rows,cols): 
    """Jacobian of differential equations for wus, ham and dimer.

    The Jacobian with f1=dwus/dt (x1=wus), f2=dham/dt (x2=ham), f3=ddimer/dt 
    (x3=dimer) is computed.

    Parameters
    ----------
    wus/ham/dim : numpy array
        Vector for wus/ham/dimer.
    gwus/gham/gdim : float
        Degradation rate of wus/ham/dimer.
    dwus/dham/ddim : float
        Diffusion constant of wus/ham/dimer.
    forw : float
        Dimerisation rate of wus and ham to form a dimer.
    back : float
        Dissociation rate of dimer to separate into wus and ham monomers.
    diag : numpy array
        Sum of sink vector and total contact surface of each cell divided by its volume.
    data : numpy array
        Vector with entries in contact matrix (contact surface/volume between 
        two cells i and j).
    rows : numpy array
        Vector with row indices of entries in contact matrix.
    cols : numpy array
        Vector with column indices of entries in contact matrix.

    Return
    ------
    jac_mat : numpy array
        Jacobian.
    jdata : numpy array
        Vector with entries in Jacobian.
    jrows : numpy array
        Vector with row indices of entries in Jacobian.
    jcols : numpy array
        Vector with column indices of entries in Jacobian.
    """
    j1wus_dia = - gwus - dwus*diag - forw*ham
    j1wus_neigh = dwus*data
    j2wus_dia = - forw*wus
    j3wus_dia = back*np.ones(len(diag))
    j1ham_dia = - forw*ham
    j2ham_dia = - gham - dham*diag - forw*wus
    j2ham_neigh = dham*data
    j3ham_dia = back*np.ones(len(diag))
    j1dim_dia = forw*ham
    j2dim_dia = forw*wus
    j3dim_dia = - gdim - ddim*diag - back
    j3dim_neigh = ddim*data
    dia_rowscols = np.arange(len(diag))
    jdata = np.concatenate((j1wus_dia,j1wus_neigh,j2wus_dia,j3wus_dia,j1ham_dia,j2ham_dia,j2ham_neigh,j3ham_dia,j1dim_dia,j2dim_dia,j3dim_dia,j3dim_neigh))  
    jrows = np.concatenate((dia_rowscols,rows,dia_rowscols,dia_rowscols,dia_rowscols+len(wus),dia_rowscols+len(wus),rows+len(wus),dia_rowscols+len(wus),dia_rowscols+2*len(wus),dia_rowscols+2*len(wus),dia_rowscols+2*len(wus),rows+2*len(wus)))
    jcols = np.concatenate((dia_rowscols,cols,dia_rowscols+len(wus),dia_rowscols+2*len(wus),dia_rowscols,dia_rowscols+len(wus),cols+len(wus),dia_rowscols+2*len(wus),dia_rowscols,dia_rowscols+len(wus),dia_rowscols+2*len(wus),cols+2*len(wus)))
    jac_mat = sc.sparse.coo_matrix((jdata,(jrows,jcols)),shape=(3*len(wus),3*len(wus)))
    jac_mat = jac_mat.tocsc()
    return jac_mat,jdata,jrows,jcols
    

def vec(W,H,wus,ham,dim,pwus,gwus,dwus,pham,gham,dham,gdim,ddim,forw,back,diag,jac,jdata,jrows,jcols,data,rows,cols): 
    """Vector Jac*x(n)-f is computed.

    The vector Jac*x(n)-f where x=wus,ham,dim and f=dwus/dt,dham/dt,ddim/dt is computed to find f=0 using Newton's method. 
    Jac * \Delta x = -f, \Delta x = x(n+1)-x(n) -> Jac*x(n+1) = Jac*x(n) - f 

    Parameters
    ----------
    W/H : numpy array
        Vector with computed WUS/HAM.
    wus/ham/dim : numpy array
        Vector with computed wus/ham/dimer.
    pwus/pham : float
        Production rate of wus/ham.
    gwus/gham/gdim : float
        Degradation rate of wus/ham/dimer.
    dwus/dham/ddim : float
        Diffusion constant of wus/ham/dimer.
    forw : float
        Dimerisation rate of wus and ham to form a dimer.
    back : float
        Dissociation rate of dimer to separate into wus and ham monomers.
    diag : numpy array
        Sum of sink vector and total contact surface of each cell divided by its volume.
    jac : numpy array
        Jacobian of differential equations for wus, ham and dimer.
    jdata : numpy array
        Vector with entries in Jacobian.
    jrows : numpy array
        Vector with row indices of entries in Jacobian.
    jcols : numpy array
        Vector with column indices of entries in Jacobian.
    data : numpy array
        Vector with entries in contact matrix (contact surface/volume between 
        two cells i and j).
    rows : numpy array
        Vector with row indices of entries in contact matrix.
    cols : numpy array
        Vector with column indices of entries in contact matrix.

    Return
    ------
    vec : numpy array
        Vector Jac*x(n)-f.
    """
    x = np.concatenate((wus,ham,dim))
    vec0 = np.zeros(len(x))
    for i in range(len(jrows)):
        vec0[jrows[i]] += jdata[i]*x[jcols[i]]
    fwus = pwus*W - gwus*wus - dwus*diag*wus - forw*ham*wus + back*dim
    fham = pham*H - gham*ham - dham*diag*ham - forw*ham*wus + back*dim
    fdim = -gdim*dim - ddim*diag*dim + forw*ham*wus - back*dim
    for i in range(len(rows)):
        fwus[rows[i]] += dwus*data[i]*wus[cols[i]]
        fham[rows[i]] += dham*data[i]*ham[cols[i]]
        fdim[rows[i]] += ddim*data[i]*dim[cols[i]]
    f = np.concatenate((fwus,fham,fdim))
    vec = vec0 - f
    return vec 

    
def read_contacts_real(file_in,nb_cells):
    """Contact matrix is read from a file.
    
    Parameters
    ----------
    file_in : string
        Filename.
    nb_cells : integer
        Number of cells.
        
    Return
    ------
    Contacts : numpy array
        Matrix with contact surface between cells i and j.    
    """
    chan = open(file_in,"r")
    lines = chan.readlines()
    chan.close()
    nb_cells = lines[0].split(" ")
    nb_cells = int(nb_cells[0])
    Contacts = np.zeros((nb_cells,nb_cells))
    for i,l in enumerate(lines[1:]):
        l = l.replace(",","")
        if l!="\n":
            s = l[:-1].split(" ")
            while "" in s: s.remove("")
            if int(s[0]) != i: raise NameError('Problem with neighborhood file, line '+str(i))
            nb_neighs = int(s[1])
            neighs = [int(e) for e in s[2:2+nb_neighs]]
            contacts = [float(e) for e in s[2+nb_neighs:2+nb_neighs*2]]
            for c,n in enumerate(neighs):
                Contacts[i][n] = contacts[c]
    return Contacts


def print_full_params(file_in,params):
    chan = open(file_in,"a")
    for p in params:
        chan.write(str(p)+" ")
    chan.write("\n")
    chan.close()


def read_params(file_in,line):
    chan = open(file_in,"r")
    lines = chan.readlines()
    chan.close()
    l = lines[line]
    params = l[:-1].split(" ")
    while "" in params:
        params.remove("")
    params = [float(e) for e in params]
    return params
    

def write_init(f,columns,newman=False):
    chan = open(f,"w")
    if newman: chan.write("1\n%i %i 0\n" % (len(columns[0]),len(columns)))
    else: chan.write("%i %i\n" % (len(columns[0]),len(columns)))
    for i in range(0,len(columns[0])):
        for c in columns:
            chan.write("%g " % c[i])
        chan.write("\n")
    chan.close()
