#######################
### Optimisation script
# 1) Optimisation of the WUS domain
# 2) Optimisation of CLV3/HAM domains
# 3) Optimisation of CLV3 peptide


import os
import sys
from matplotlib import pyplot as pp
from routines import *
import numpy as np
from scipy.optimize import minimize
from time import gmtime, strftime

job = sys.argv[1]

#### Builds the various elements required for computation and plotting

#dimension of the grid and radius of the meristem
dim = 50
rad = 30
#N: neighborhood matrix
#L: L1 matrix
#S: Sink matrix
#to_flat, to_square: conversion methods between matrices used for display/building and vectors used for computations
#dim2: length of the "flat" meristem vectors (all the computation vectors) = nb of cells in the meristem
D,N,L,S,dim2,to_flat,to_square = build_DNLS(rad,dim)
#neighborhood, sink and L1 vectors
fN, fS, fL= flatten(N,dim,dim2,to_square), flatten(S,dim,dim2,to_square), flatten(L,dim,dim2,to_square)

#optimisation targets for WUS, CLV3 and HAM
WT = np.zeros((dim,dim)); CT = np.zeros((dim,dim)); HT = np.zeros((dim,dim));
for i in range(dim):
    for j in range(dim):
        if j**2 + (i-15)**2 <= 5**2: WT[i,j] = 1
        if j**2 + (i-30)**2 <= 10**2: CT[i,j] = 1
        if j**2 + (i+10)**2 <= 30**2: HT[i,j] = 1
fWT = flatten(WT,dim,dim2,to_square)
fCT = flatten(CT,dim,dim2,to_square)
fHT = flatten(HT,dim,dim2,to_square)

#coordinates for sparse matrices
rows, cols, data, dia = build_topo(dim,to_square,to_flat,D,fN,fS)
n = 4
levels = np.arange(0,2,2/100.)

plot_list = []
all_bounds = 8

vW = 2; ncyt = 8; nahk = 4; ncW = 2
vH = 1; nlh = 4

# ###########
# ### WUS ###
# ###########

# computes stable WUS in CLV3 loss of function
def stable_bW(params):
    kcyt, kahk,pcyt,dcyt,gcyt,pahk,dahk,gahk = params
    cyt = pdg(pcyt*fL,dcyt,gcyt,dia,rows,cols,data)
    ahk = pdg(pahk*fL,dahk,gahk,dia,rows,cols,data)
    bW = vW * (cyt**ncyt / (cyt**ncyt + kcyt**ncyt)) * (kahk**nahk / (ahk**nahk + kahk**nahk))
    return bW,cyt,ahk

# cost function WUS in CLV3 loss of function
def f_bW(params):
    bW,cyt,ahk = stable_bW(params)
    diff = np.sum((bW-(fWT*1.5))**2)
    print "bW", diff
    return diff

# optimisation of WUS in CLV3 loss of function
bWparams = optimize(f_bW,8,184,56,bounds = all_bounds, max_count = 10000)
bW,cyt,ahk = stable_bW(bWparams)
kcyt, kahk,pcyt,dcyt,gcyt,pahk,dahk,gahk = bWparams

# computes stable WUS in wild type
def stable_sW(params):
    kcW, pc, dc= params
    c = pdg(pc*fCT,dc,1,dia,rows,cols,data)
    sW = bW * (kcW**ncW / (c**ncW + kcW**ncW))
    return sW,c

# cost function and optimisation for WUS in wild type
def f_sW(params):
    sW,c = stable_sW(params)
    diff = np.sum((sW-(bW*0.8))**2)
    print "sW", diff
    return diff

sWparams = optimize(f_sW,3,400,1,bounds = all_bounds)
W,c = stable_sW(sWparams)
kcW, pc, dc= sWparams

plot_list.append((fWT,"fWT",False))
plot_list.append((W,"W",False))
plot_list.append((bW,"bW",False))


############
### CLV3 ###
############

# Shea-Ackers for CLV3 expression
def shea(W,D,kw,kd):
    return (kw*W) / (1 + kw*W + kd*D)

# derivatives of the monomers/dimers
def f(a,b,d,A,B,pa,ga,da,pb,gb,db,gd,dd,forw,back):
    return np.concatenate(
        (another_deriv_x(pa*A+back*d,da,ga+forw*b,a,rows,cols,dia),
         another_deriv_x(pb*B+back*d,db,gb+forw*a,b,rows,cols,dia),
         another_deriv_x(forw*a*b,dd,gd+back,d,rows,cols,dia)))

# jacobian of the monomers/dimers
def j(a,b,d,pa,ga,da,pb,gb,db,gd,dd,forw,back):
    dia_a = -ga -da*dia -forw*b
    dia_b = -gb -db*dia -forw*a
    dia_d = -gd -dd*dia -back
    
    dia_ab = -forw*a
    dia_ad = np.full(dim2,back)

    dia_ba = -forw*b
    dia_bd = np.full(dim2,back)

    dia_da = forw*b
    dia_db = forw*a

    dia_rowsncols = np.arange(dim2)
    diff_data = -data

    ldata = np.concatenate((
        dia_a,#dia_a
        dia_b,#dia_b
        dia_d,#dia_d
        diff_data*da,#neigh_a
        diff_data*db,#neigh_b
        diff_data*dd,#neigh_d
        dia_ab,
        dia_ad,
        dia_ba,
        dia_bd,
        dia_da,
        dia_db))

    lrows = np.concatenate((
        dia_rowsncols,#dia_a
        dia_rowsncols+dim2,#dia_b
        dia_rowsncols+dim2*2,#dia_d
        rows,#neigh_a
        rows+dim2,#neigh_b
        rows+dim2*2,#neigh_d
        dia_rowsncols,#dia_ab
        dia_rowsncols,#dia_ad
        dia_rowsncols+dim2,#dia_ba
        dia_rowsncols+dim2,#dia_bd
        dia_rowsncols+dim2*2,#dia_da
        dia_rowsncols+dim2*2))#dia_db

    lcols = np.concatenate((
        dia_rowsncols,#dia_a
        dia_rowsncols+dim2,#dia_b
        dia_rowsncols+dim2*2,#dia_d
        cols,#neigh_a
        cols+dim2,#neigh_b
        cols+dim2*2,#neigh_d
        dia_rowsncols+dim2,#dia_ab
        dia_rowsncols+dim2*2,#dia_ad
        dia_rowsncols,#dia_ba
        dia_rowsncols+dim2*2,#dia_bd
        dia_rowsncols,#dia_da
        dia_rowsncols+dim2))#dia_db

    M = sp.coo_matrix((ldata,(lrows,lcols)),shape=(dim2*3,dim2*3))
    return M.tocsc()

#stores ham monomers, wus monomers and dimers for wild, clv3 loss of function and ham loss of function
h,w,d = np.zeros(dim2),np.zeros(dim2),np.zeros(dim2)
h1,w1,d1 = np.zeros(dim2),np.zeros(dim2),np.zeros(dim2)
h2,w2,d2 = np.zeros(dim2),np.zeros(dim2),np.zeros(dim2)

# computes stable state of CLV3, HAM and the monomers/dimers
# mode # 0: wild type, 1: clv3 loss of function, 2: ham loss of function
def stable_C(W,params,mode=0,limit=20):
    klh,plh,dlh,pw,dw,ph,dh,forw,dd,vC,kw,kd,gh, gw, gd, back = params
    lh = pdg(plh*fL,dlh,1,dia,rows,cols,data)
    H = vH * (klh**nlh / (lh**nlh + klh**nlh))
    global h,w,d; global h1,w1,d1; global h2,w2,d2
    if mode == 0: local_h,local_w,local_d = h,w,d
    if mode == 1: local_h,local_w,local_d = h1,w1,d1; pw*=1.5
    if mode == 2: local_h,local_w,local_d = h2,w2,d2; ph=0
    stable = 10000
    i_stable = 0;
    #stabilisation with Newton method
    while stable>1e-10 and i_stable <= limit:
        i_stable+=1
        print ".",
        g = f(local_w,local_h,local_d,W,H,pw,gw,dw,ph,gh,dh,gd,dd,forw,back)
        M = j(local_w,local_h,local_d,pw,gw,dw,ph,gh,dh,gd,dd,forw,back)
        v = sp.linalg.spsolve(M,-g)
        x2 = np.concatenate((local_w,local_h,local_d))+v

        for delme in range(x2.shape[0]-1):
            if x2[delme]<0: x2[delme] = 0
        
        stable = np.sum(v**2)
        local_w,local_h,local_d = x2[:dim2],x2[dim2:dim2*2],x2[dim2*2:dim2*3]
        
    if i_stable >= limit:
        local_w,local_h,local_d =np.zeros(dim2),np.zeros(dim2),np.zeros(dim2)
    C = vC * shea(local_w, local_d, kw, kd)

    if mode == 0: h,w,d = local_h,local_w,local_d
    if mode == 1: h1,w1,d1 = local_h,local_w,local_d
    if mode == 2: h2,w2,d2 = local_h,local_w,local_d
    return H,lh,local_h,local_w,local_d,C,i_stable

# cost function for CLV3 and HAM in wild type
def f_C(params):
    global count_opt; count_opt +=1
    H,lh,h,w,d,C,i_stable = stable_C(W,params)
    diff1 = np.sum((C-fCT)**2)
    diff2 = np.sum((H-fHT)**2)/12.5
    diff =  diff1 + diff2
    print "\n",count_opt,"C", diff, diff1, diff2
    params_s = ["klh","plh","dlh","pw","dw","ph","dh","forw","dd","vC","kw","kd","gh", "gw", "gd", "back"]
    for e in zip(params_s,params): print e
    return diff

# cost function for CLV3 and HAM in wild type + clv3 loss of function + ham loss of function
def f_C2(params):
    global count_opt; count_opt +=1
    H,_,_,_,_,C,_ = stable_C(W,params); print "wild",
    _,_,_,_,_,C1,_ = stable_C(W,params,mode=1); print "Clof",
    _,_,_,_,_,C2,_ = stable_C(W,params,mode=2,limit=100); print "Hlof",
    diff1 = np.sum((C-fCT)**2)
    diff2 = np.sum((H-fHT)**2)/25
    diff3 = (((np.sum(C1)/np.sum(C))-1.5)**2) / 5
    diff4 = np.log(np.sum((W-C2)**2))
    # diff4 = np.sum((W-C2)**2) / 500
    diff =  diff1 + diff2 + diff3 + diff4
    print "\n",count_opt,"C", diff, diff1, diff2, diff3, diff4
    if diff != diff: sys.exit() 
    return diff

count_opt=0
Cparams = optimize(f_C,16,9999999,20,bounds = all_bounds) # 1st thr 77
Cparams_a = Cparams.copy()

bounds = [(10**-all_bounds,10**all_bounds) for e in range(16)]
Cparams = minimize(f_C2,Cparams,method='L-BFGS-B',bounds=bounds).x

klh,plh,dlh,pw,dw,ph,dh,forw,dd,vC,kw,kd,gh, gw, gd, back = Cparams
H,lh,h,w,d,C,_ = stable_C(W,Cparams)

############
### clv3 ###
############

#optimisation of clv3 peptide against its gradient in step 1
cT = c.copy()

def stable_c(params):
    pc,dc,gc = params
    c = pdg(pc*C,dc,gc,dia,rows,cols,data)
    return c

def f_c(params):
    c = stable_c(params)
    diff = np.sum((c-cT)**2); print diff
    return diff

cparams = optimize(f_c,3,10000000,1000000,bounds = all_bounds)
c = stable_c(cparams)
pc,dc,gc = cparams

#saves optimised parameters
os.popen("mkdir OPT")
chan=open("OPT/params_"+str(job),"w")
for p in bWparams: chan.write(str(p)+" ")
chan.write("\n")
for p in sWparams: chan.write(str(p)+" ")
chan.write("\n")
for p in Cparams: chan.write(str(p)+" ")
chan.write("\n")
for p in cparams: chan.write(str(p)+" ")
chan.write("\n")
chan.close()
